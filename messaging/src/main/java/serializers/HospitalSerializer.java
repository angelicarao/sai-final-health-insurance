package serializers;

import com.google.gson.Gson;
import hospital.model.HospitalCostsReply;
import hospital.model.HospitalCostsRequest;

/**
 * Provides functionality for serializing and de-serializing {@link HospitalCostsRequest}
 * and {@link HospitalCostsReply} objects.
 */
public class HospitalSerializer {
    private Gson gson;

    public HospitalSerializer() {
        this.gson = new Gson();
    }

    /**
     * Converts a {@link HospitalCostsRequest} to a JSON string.
     *
     * @param hospitalCostsRequest The request to convert.
     * @return Returns a JSON string of the request.
     */
    public String requestToString(HospitalCostsRequest hospitalCostsRequest) {
        return this.gson.toJson(hospitalCostsRequest);
    }

    /**
     * Converts a JSON string to a {@link HospitalCostsRequest}.
     *
     * @param requestJson The JSON string to convert.
     * @return Returns a {@link HospitalCostsRequest} object.
     */
    public HospitalCostsRequest requestFromString(String requestJson) {
        return this.gson.fromJson(requestJson, HospitalCostsRequest.class);
    }

    /**
     * Converts a {@link HospitalCostsReply} to a JSON string.
     *
     * @param hospitalCostsReply The reply to convert.
     * @return Returns a JSON string of the reply.
     */
    public String replyToString(HospitalCostsReply hospitalCostsReply) {
        return this.gson.toJson(hospitalCostsReply);
    }

    /**
     * Converts a JSON string to a {@link HospitalCostsReply}.
     *
     * @param replyJson The JSON string to convert.
     * @return Returns a {@link HospitalCostsReply} object.
     */
    public HospitalCostsReply replyFromString(String replyJson) {
        return this.gson.fromJson(replyJson, HospitalCostsReply.class);
    }
}
