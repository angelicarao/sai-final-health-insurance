package serializers;

import com.google.gson.Gson;
import insurance.model.*;

/**
 * Provides functionality for serializing and de-serializing {@link TreatmentCostsRequest}
 * and {@link TreatmentCostsReply} objects.
 */
public class InsuranceSerializer {
    private Gson gson;

    public InsuranceSerializer() {
        this.gson = new Gson();
    }

    /**
     * Converts a {@link TreatmentCostsRequest} to a JSON string.
     *
     * @param treatmentCostsRequest The request to convert.
     * @return Returns a JSON string of the request.
     */
    public String requestToString(TreatmentCostsRequest treatmentCostsRequest) {
        return this.gson.toJson(treatmentCostsRequest);
    }

    /**
     * Converts a JSON string to a {@link TreatmentCostsRequest}.
     *
     * @param requestJson The JSON string to convert.
     * @return Returns a {@link TreatmentCostsRequest} object.
     */
    public TreatmentCostsRequest requestFromString(String requestJson) {
        return this.gson.fromJson(requestJson, TreatmentCostsRequest.class);
    }

    /**
     * Converts a {@link TreatmentCostsReply} to a JSON string.
     *
     * @param treatmentCostsReply The reply to convert.
     * @return Returns a JSON string of the reply.
     */
    public String replyToString(TreatmentCostsReply treatmentCostsReply) {
        return this.gson.toJson(treatmentCostsReply);
    }

    /**
     * Converts a JSON string to a {@link TreatmentCostsReply}.
     *
     * @param replyJson The JSON string to convert.
     * @return Returns a {@link TreatmentCostsReply} object.
     */
    public TreatmentCostsReply replyFromString(String replyJson) {
        return this.gson.fromJson(replyJson, TreatmentCostsReply.class);
    }
}
