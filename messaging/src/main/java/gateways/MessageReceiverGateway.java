package gateways;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.naming.NamingException;

/**
 * A {@link MessageGateway} which provides functionality to receive messages.
 */
public class MessageReceiverGateway extends MessageGateway {
    public MessageConsumer consumer;

    public MessageReceiverGateway(String queueName) throws NamingException, JMSException {
        super(queueName);
        this.consumer = this.session.createConsumer(this.destination);
    }

    /**
     * Sets a listener to the message consumer.
     *
     * @param messageListener The listener to set.
     * @throws JMSException Thrown when a JMS exception occurs.
     */
    public void setListener(MessageListener messageListener) throws JMSException {
        this.consumer.setMessageListener(messageListener);
        this.connection.start();
    }
}