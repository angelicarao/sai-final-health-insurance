package gateways;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.naming.NamingException;
import javax.jms.Message;

/**
 * A {@link MessageGateway} which provides functionality for creating and sending messages.
 */
public class MessageSenderGateway extends MessageGateway {
    public MessageProducer producer;

    public MessageSenderGateway(String queueName) throws NamingException, JMSException {
        super(queueName);
        this.producer = this.session.createProducer(this.destination);
    }

    /**
     * Creates a text {@link Message} with the provided body and sets the JMS correlation ID
     * to the provided one.
     *
     * @param body          The body of the message.
     * @param correlationId The correlation identifier for the message.
     * @return Returns a {@link Message} object.
     * @throws JMSException Thrown when a JMS exception occurs.
     */
    public Message createTextMessage(String body, String correlationId) throws JMSException {
        Message msg = this.session.createTextMessage(body);
        msg.setJMSCorrelationID(correlationId);
        return msg;

    }

    /**
     * Creates a text {@link Message} with the provided body.
     *
     * @param body The body of the message.
     * @return Returns a {@link Message} object.
     * @throws JMSException Thrown when a JMS exception occurs.
     */
    public Message createTextMessage(String body) throws JMSException {
        Message msg = this.session.createTextMessage(body);
        return msg;

    }

    /**
     * Sends a message to the provided destination.
     *
     * @param message     The message to send.
     * @param destination The destination where the message should be sent.
     * @throws JMSException Thrown when a JMS exception occurs.
     */
    public void send(Message message, Destination destination) throws JMSException {
        this.producer.send(destination, message);
    }

    /**
     * Sends a message to the destination of this gateway.
     *
     * @param message The message to send.
     * @throws JMSException Thrown when a JMS exception occurs.
     */
    public void send(Message message) throws JMSException {
        this.producer.send(message);
    }
}
