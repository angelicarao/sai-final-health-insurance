package gateways;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

/**
 * A generic message gateway used for initializing common properties for a
 * JMS message exchange.
 */
public class MessageGateway {
    protected javax.jms.Connection connection;
    protected javax.jms.Session session;
    protected java.util.Properties properties;
    protected Context jndiContext;
    protected javax.jms.ConnectionFactory connectionFactory;
    public javax.jms.Destination destination;

    public MessageGateway(String queueName) throws NamingException, JMSException {
        initProperties(queueName);
        jndiContext = new InitialContext(properties);
        connectionFactory = (ConnectionFactory) jndiContext
                .lookup("ConnectionFactory");
        connection = connectionFactory.createConnection();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        // connect to the sender destination
        destination = (Destination) jndiContext.lookup(queueName);
    }

    private void initProperties(String queue) {
        properties = new Properties();
        properties.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
        properties.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616");
        properties.put(("queue." + queue), queue);
    }
}
