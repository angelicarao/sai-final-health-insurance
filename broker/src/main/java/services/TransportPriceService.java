package services;

import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.*;
import javax.ws.rs.core.*;
import java.net.URI;

/**
 * An HTTP service used to retrieve the transport price per kilometer.
 */
public class TransportPriceService {
    private URI baseUri;
    private Client client;

    public TransportPriceService() {
        this.baseUri = UriBuilder.fromUri("http://localhost:8080").build();
        this.client = ClientBuilder.newClient(new ClientConfig());
    }

    /**
     * Retrieves the transport price per kilometer.
     *
     * @return Returns a double representing the transport price per kilometer.
     */
    public double getPricePerKm() {
        WebTarget target = client.target(baseUri + "/transportprice(Java10)/rest/price");
        Invocation.Builder requestBuilder = target.request().accept(MediaType.TEXT_PLAIN);
        Response response = requestBuilder.get();

        if (response.getStatus() == 200) {
            Double pricePerKm = response.readEntity(Double.class);
            System.out.println("Retrieved price per km: " + pricePerKm);
            return pricePerKm;
        } else {
            System.out.println("Price per km could not be retrieved.");
            return 0.00;
        }
    }
}
