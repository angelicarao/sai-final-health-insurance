package messaging;

import gateways.MessageReceiverGateway;
import gateways.MessageSenderGateway;
import hospital.model.HospitalCostsReply;
import hospital.model.HospitalCostsRequest;
import insurance.model.TreatmentCostsRequest;
import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;
import net.sourceforge.jeval.function.math.Max;
import serializers.HospitalSerializer;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An application gateway providing functionality to communicate with the hospital applications.
 */
public class HospitalApplicationGateway {
    public MessageSenderGateway senderGatewayCatharina;
    public MessageSenderGateway senderGatewayMaxima;
    public MessageSenderGateway senderGatewayUMC;
    public MessageReceiverGateway receiverGateway;
    public Aggregator aggregator;
    private HospitalSerializer hospitalSerializer;
    private ConcurrentHashMap<String, TreatmentCostsRequest> idToRequestHashMap;

    // Routing rules
    private final String Catharina = "#{age} >= 10 && startsWith('#{code}', 'ORT', 0)";
    private final String Maxima = "#{age} >= 18";
    private Evaluator evaluator;

    /**
     * Initializes a new instance of class {@link HospitalApplicationGateway}. Sets a listener to wait
     * for {@link HospitalCostsReply} messages.
     *
     * @throws JMSException
     * @throws NamingException
     */
    public HospitalApplicationGateway() throws JMSException, NamingException {
        this.senderGatewayCatharina = new MessageSenderGateway("requestHospitalCatharina");
        this.senderGatewayMaxima = new MessageSenderGateway("requestHospitalMaxima");
        this.senderGatewayUMC = new MessageSenderGateway("requestHospitalUMC");
        this.receiverGateway = new MessageReceiverGateway("replyHospital");
        this.hospitalSerializer = new HospitalSerializer();
        this.idToRequestHashMap = new ConcurrentHashMap<>();
        this.evaluator = new Evaluator();
        this.aggregator = new Aggregator();

        this.receiverGateway.setListener(hospitalReplyMessage -> {
            try {
                HospitalCostsReply reply = this.hospitalSerializer
                        .replyFromString(((TextMessage) hospitalReplyMessage).getText());
                System.out.println("Received message from replyHospital: " + reply.toString());

                TreatmentCostsRequest initialRequest = this.idToRequestHashMap
                        .get(hospitalReplyMessage.getStringProperty("aggregationId"));

                // Trigger callback for when a hospital reply has arrived.
                this.onHospitalReplyReceived(initialRequest, reply);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Sends a {@link HospitalCostsRequest}.
     *
     * @param hospitalCostsRequest The request to send.
     * @param toRequest            The {@link TreatmentCostsRequest} to which the hospital request corresponds.
     * @param replyTo              The destinatio to set as reply to on the message.
     * @throws EvaluationException Thrown by Jeval.
     * @throws JMSException        Thrown by JMS.
     */
    public void sendMessage(HospitalCostsRequest hospitalCostsRequest, TreatmentCostsRequest toRequest, Destination replyTo)
            throws EvaluationException, JMSException {
        // Set up rule evaluator
        evaluator.putVariable("age", Integer.toString(hospitalCostsRequest.getAge()));
        evaluator.putVariable("code", hospitalCostsRequest.getTreatmentCode());
        String result = evaluator.evaluate(Catharina);
        boolean catharinaRule = result.equals("1.0");
        result = evaluator.evaluate(Maxima);
        boolean maximaRule = result.equals("1.0");

        int expectedReplies = catharinaRule ? 1 : 0;
        expectedReplies += maximaRule ? 1 : 0;
        expectedReplies++; // Always add one for UMC

        String aggregationId = this.aggregator.addTreatmentRequest(toRequest, expectedReplies);

        // Check to which bank the request has to be sent
        if (catharinaRule) {
            this.sendHospitalRequestCatharina(hospitalCostsRequest, toRequest, replyTo, aggregationId);
        }
        if (maximaRule) {
            this.sendHospitalRequestMaxima(hospitalCostsRequest, toRequest, replyTo, aggregationId);
        }
        // Always send to UMC as they process all requests
        this.sendHospitalRequestUMC(hospitalCostsRequest, toRequest, replyTo, aggregationId);
    }

    /**
     * A callback triggered when a {@link HospitalCostsReply} has been received.
     *
     * @param request The request to which this reply belongs.
     * @param reply   The received reply.
     */
    public void onHospitalReplyReceived(TreatmentCostsRequest request, HospitalCostsReply reply) {
        System.out.println("Received hospital reply: " + reply.toString());
    }

    private void sendHospitalRequestCatharina(
            HospitalCostsRequest hospitalCostsRequest,
            TreatmentCostsRequest toRequest,
            Destination replyTo,
            String aggregationId) throws JMSException {
        Message message = this.senderGatewayCatharina.createTextMessage(
                this.hospitalSerializer.requestToString(hospitalCostsRequest));
        message.setJMSReplyTo(replyTo);
        message.setStringProperty("aggregationId", aggregationId);
        this.senderGatewayCatharina.send(message);
        this.idToRequestHashMap.put(aggregationId, toRequest);

        System.out.println("Sending request to Catharina hospital: " + hospitalCostsRequest.toString());
    }

    private void sendHospitalRequestMaxima(
            HospitalCostsRequest hospitalCostsRequest,
            TreatmentCostsRequest toRequest,
            Destination replyTo,
            String aggregationId) throws JMSException {
        Message message = this.senderGatewayMaxima.createTextMessage(
                this.hospitalSerializer.requestToString(hospitalCostsRequest));
        message.setJMSReplyTo(replyTo);
        message.setStringProperty("aggregationId", aggregationId);
        this.senderGatewayMaxima.send(message);
        this.idToRequestHashMap.put(aggregationId, toRequest);

        System.out.println("Sending request to Maxima Medical Center: " + hospitalCostsRequest.toString());
    }

    private void sendHospitalRequestUMC(
            HospitalCostsRequest hospitalCostsRequest,
            TreatmentCostsRequest toRequest,
            Destination replyTo,
            String aggregationId) throws JMSException {
        Message message = this.senderGatewayUMC.createTextMessage(
                this.hospitalSerializer.requestToString(hospitalCostsRequest));
        message.setJMSReplyTo(replyTo);
        message.setStringProperty("aggregationId", aggregationId);
        this.senderGatewayUMC.send(message);
        this.idToRequestHashMap.put(aggregationId, toRequest);

        System.out.println("Sending request to UMC: " + hospitalCostsRequest.toString());
    }
}
