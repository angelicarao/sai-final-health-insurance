package messaging;

import hospital.model.HospitalCostsReply;
import insurance.model.TreatmentCostsRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a treatment request together with its ID, expected and actual replies.
 */
public class Aggregation {
    public String aggregationId;
    public TreatmentCostsRequest treatmentCostsRequest;
    public int expectedReplies;
    public List<HospitalCostsReply> actualReplies;

    public Aggregation(String aggregationId, TreatmentCostsRequest loanRequest, int expectedReplies) {
        this.aggregationId = aggregationId;
        this.treatmentCostsRequest = loanRequest;
        this.expectedReplies = expectedReplies;
        this.actualReplies = new ArrayList<>();
    }
}
