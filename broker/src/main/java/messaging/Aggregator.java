package messaging;

import hospital.model.HospitalCostsReply;
import insurance.model.TreatmentCostsRequest;

import java.util.*;

/**
 * Provides functionality for collecting and storing aggregations.
 */
public class Aggregator {
    private List<Aggregation> aggregations;

    public Aggregator() {
        this.aggregations = new ArrayList<>();
    }

    /**
     * Adds a new treatment request and its expected replies to the collection of aggregations.
     *
     * @param treatmentCostsRequest The request to add.
     * @param expectedReplies       The number of expected replies for this request.
     * @return Returns a generated unique aggregationId for this request.
     */
    public String addTreatmentRequest(TreatmentCostsRequest treatmentCostsRequest, int expectedReplies) {
        String aggregationId = UUID.randomUUID().toString();

        Aggregation aggregation = new Aggregation(aggregationId, treatmentCostsRequest, expectedReplies);
        this.aggregations.add(aggregation);

        return aggregationId;
    }

    /**
     * Callback triggered when a new reply has arrived for a treatment request. If the expected
     * replies are as much as the actual ones, true is returned, else false.
     *
     * @param request The request for which a reply arrived.
     * @param reply   The reply for the request.
     * @return A boolean indicating whether the actual replies match the expected ones.
     */
    public boolean onReplyArrived(TreatmentCostsRequest request, HospitalCostsReply reply) {
        for (Aggregation aggregation : this.aggregations) {
            if (request.equals(aggregation.treatmentCostsRequest)) {
                aggregation.actualReplies.add(reply);

                if (aggregation.actualReplies.size() == aggregation.expectedReplies) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Retrieves the best reply for a provided request.
     *
     * @param request The request for which a reply should be retrieved.
     * @return Returns a {@link HospitalCostsReply} which offers the lowest treatment price.
     */
    public HospitalCostsReply getBestReply(TreatmentCostsRequest request) {
        for (Aggregation aggregation : this.aggregations) {
            if (request.equals(aggregation.treatmentCostsRequest)) {
                HospitalCostsReply bestReply = Collections.min(aggregation.actualReplies, Comparator.comparing(c -> c.getPrice()));

                return bestReply;
            }
        }

        return null;
    }
}
