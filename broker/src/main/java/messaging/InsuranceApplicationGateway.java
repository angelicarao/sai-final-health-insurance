package messaging;

import gateways.MessageReceiverGateway;
import gateways.MessageSenderGateway;
import insurance.model.TreatmentCostsReply;
import insurance.model.TreatmentCostsRequest;
import serializers.InsuranceSerializer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An application gateway providing functionality to communicate with the insurance client queues.
 */
public class InsuranceApplicationGateway {
    public MessageSenderGateway messageSenderGateway;
    public MessageReceiverGateway messageReceiverGateway;
    public InsuranceSerializer insuranceSerializer;
    private ConcurrentHashMap<TreatmentCostsRequest, Message> requestToMessageHashMap;

    /**
     * Initializes a new instance of class {@link InsuranceApplicationGateway}. Sets a message
     * listener which deals with received {@link TreatmentCostsRequest} and
     * {@link TreatmentCostsReply}.
     *
     * @throws JMSException    Thrown when an exception occurs in JMS.
     * @throws NamingException Thrown by JMS.
     */
    public InsuranceApplicationGateway() throws JMSException, NamingException {
        this.messageSenderGateway = new MessageSenderGateway("replyInsuranceClient");
        this.messageReceiverGateway = new MessageReceiverGateway("requestInsuranceClient");
        this.insuranceSerializer = new InsuranceSerializer();
        this.requestToMessageHashMap = new ConcurrentHashMap<>();

        this.messageReceiverGateway.setListener(treatmentCostsRequestMessage -> {
            try {
                TreatmentCostsRequest request = this.insuranceSerializer
                        .requestFromString(((TextMessage) treatmentCostsRequestMessage).getText());
                this.requestToMessageHashMap.put(request, treatmentCostsRequestMessage);

                // Trigger callback with received request.
                this.onTreatmentRequestReceived(request);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * A callback triggered when a treatment reply is received.
     *
     * @param request The request for which the reply is.
     * @param reply   The reply to the request.
     * @throws JMSException Thrown when an exception occurs in JMS.
     */
    public void onTreatmentReplyReceived(TreatmentCostsRequest request, TreatmentCostsReply reply) throws JMSException {
        Message originalRequest = this.requestToMessageHashMap.get(request);
        Message treatmentReplyMessage = this.messageSenderGateway
                .createTextMessage(this.insuranceSerializer.replyToString(reply), originalRequest.getJMSMessageID());
        this.messageSenderGateway.send(treatmentReplyMessage, originalRequest.getJMSReplyTo());
    }

    /**
     * A callback triggered when a treatment request is received.
     *
     * @param request The requedt which has been received.
     */
    public void onTreatmentRequestReceived(TreatmentCostsRequest request) {
        System.out.println("Received treatment request: " + request.toString());
    }
}
