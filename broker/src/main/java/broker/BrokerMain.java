package broker;

import hospital.model.HospitalCostsReply;
import hospital.model.HospitalCostsRequest;
import insurance.model.TreatmentCostsReply;
import insurance.model.TreatmentCostsRequest;
import messaging.HospitalApplicationGateway;
import messaging.InsuranceApplicationGateway;
import net.sourceforge.jeval.EvaluationException;
import services.TransportPriceService;

import javax.jms.JMSException;
import javax.naming.NamingException;

/**
 * A message broker responsible for distributing messages between queues from an insurance and
 * hospital applications.
 */
public class BrokerMain {
    private static InsuranceApplicationGateway insuranceApplicationGateway;
    private static HospitalApplicationGateway hospitalApplicationGateway;
    private static TransportPriceService transportPriceService;

    public static void main(String[] args) {
        System.out.println("Broker started");

        transportPriceService = new TransportPriceService();

        try {
            insuranceApplicationGateway = new InsuranceApplicationGateway() {
                @Override
                public void onTreatmentRequestReceived(TreatmentCostsRequest request) {
                    super.onTreatmentRequestReceived(request);

                    HospitalCostsRequest hospitalCostsRequest = new HospitalCostsRequest();
                    hospitalCostsRequest.setAge(request.getAge());
                    hospitalCostsRequest.setSsn(request.getSsn());
                    hospitalCostsRequest.setTreatmentCode(request.getTreatmentCode());

                    // Forward treatment request as hospital request
                    try {
                        hospitalApplicationGateway.sendMessage(
                                hospitalCostsRequest, request, hospitalApplicationGateway.receiverGateway.destination);
                    } catch (EvaluationException e) {
                        e.printStackTrace();
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }

                }
            };

            hospitalApplicationGateway = new HospitalApplicationGateway() {
                @Override
                public void onHospitalReplyReceived(TreatmentCostsRequest request, HospitalCostsReply reply) {
                    super.onHospitalReplyReceived(request, reply);

                    // If all expected replies have been received, send the best one.
                    if (this.aggregator.onReplyArrived(request, reply)) {
                        HospitalCostsReply bestReply = this.aggregator.getBestReply(request);

                        TreatmentCostsReply treatmentCostsReply = new TreatmentCostsReply();
                        treatmentCostsReply.setHospitalName(bestReply.getHospitalName());
                        treatmentCostsReply.setHospitalPrice(bestReply.getPrice());

                        // If the original request includes transport from the patients home to the hospital, calculate
                        // the price of transport.
                        if (request.getTransportDistance() > 0) {
                            double transportPricePerKm = transportPriceService.getPricePerKm();
                            double transportPrice = transportPricePerKm * request.getTransportDistance();
                            treatmentCostsReply.setTransportPrice(transportPrice);
                        }

                        // Forward hospital reply as treatment reply.
                        try {
                            insuranceApplicationGateway.onTreatmentReplyReceived(request, treatmentCostsReply);
                        } catch (JMSException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
