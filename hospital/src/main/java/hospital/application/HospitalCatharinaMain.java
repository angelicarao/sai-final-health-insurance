package hospital.application;

import hospital.model.Address;

public class HospitalCatharinaMain extends HospitalMain {

    public HospitalCatharinaMain() throws IllegalArgumentException {
        super(new Address("Michelangelolaan", 2, "Eindhoven"), "Catharina Ziekenhuis", "requestHospitalCatharina");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
