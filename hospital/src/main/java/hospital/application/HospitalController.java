package hospital.application;

import hospital.messaging.BrokerApplicationGateway;
import hospital.model.Address;
import hospital.model.HospitalCostsReply;
import hospital.model.HospitalCostsRequest;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import javax.jms.JMSException;
import javax.naming.NamingException;
import java.net.URL;
import java.util.ResourceBundle;

class HospitalController implements Initializable {

    @FXML
    private Label lbHospital;
    @FXML
    private Label lbAddress;
    @FXML
    private TextField tfPrice;
    @FXML
    private ListView<HospitalListLine> lvRequestReply;
    @FXML
    private Button btnSendReply;

    private final String hospitalName;
    private final Address address;
    private final String hospitalRequestQueue;

    private BrokerApplicationGateway brokerApplicationGateway;

    public HospitalController(String hospitalName, Address address, String hospitalRequestQueue) {
        this.address = address;
        this.hospitalName = hospitalName;
        this.hospitalRequestQueue = hospitalRequestQueue;
    }

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String fullAddress = this.address.getStreet() + " " + this.address.getNumber() + ", " + this.address.getCity();
        this.lbAddress.setText(fullAddress);
        this.lbHospital.setText(this.hospitalName);

        btnSendReply.setOnAction(event -> {
            sendHospitalReply();
        });

        this.initializeBrokerApplicationGateway();
    }

    @FXML
    public void sendHospitalReply() {
        HospitalListLine listLine = this.lvRequestReply.getSelectionModel().getSelectedItem();
        if (listLine != null) {
            double price = Double.parseDouble(tfPrice.getText());
            HospitalCostsReply reply = new HospitalCostsReply(price, this.hospitalName, this.address);
            listLine.setReply(reply);
            Platform.runLater(() -> this.lvRequestReply.refresh());

            // Send a message with the reply.
            this.brokerApplicationGateway.sendHospitalReply(reply, listLine.getRequest());
        }
    }

    private void initializeBrokerApplicationGateway() {
        try {
            this.brokerApplicationGateway = new BrokerApplicationGateway(hospitalRequestQueue) {
                @Override
                public void onRequestReceived(HospitalCostsRequest hospitalCostsRequest) {
                    super.onRequestReceived(hospitalCostsRequest);

                    // Update UI when a new request arrives.
                    Platform.runLater(() -> {
                        HospitalController.this.lvRequestReply.getItems()
                                .add(new HospitalListLine(hospitalCostsRequest, null));
                        HospitalController.this.lvRequestReply.refresh();
                    });
                }
            };
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
