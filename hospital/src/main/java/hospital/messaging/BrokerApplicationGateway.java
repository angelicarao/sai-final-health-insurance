package hospital.messaging;

import gateways.MessageReceiverGateway;
import gateways.MessageSenderGateway;
import hospital.model.HospitalCostsReply;
import hospital.model.HospitalCostsRequest;
import serializers.HospitalSerializer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An application gateway providing functionality to communicate with a message broker.
 */
public class BrokerApplicationGateway {
    public MessageSenderGateway messageSenderGateway;
    public MessageReceiverGateway messageReceiverGateway;
    private HospitalSerializer hospitalSerializer;
    private ConcurrentHashMap<HospitalCostsRequest, String> requestToAggregationIdHashmap;

    /**
     * Initializes a new instance of class {@link BrokerApplicationGateway}. Sets a message
     * listener which deals with received {@link HospitalCostsRequest} to sent.
     *
     * @param hospitalRequestQueue The name of the queue to listen messages from.
     * @throws JMSException    Thrown when an exception occurs in JMS.
     * @throws NamingException Thrown by JMS.
     */
    public BrokerApplicationGateway(String hospitalRequestQueue) throws JMSException, NamingException {
        this.messageSenderGateway = new MessageSenderGateway("replyHospital");
        this.messageReceiverGateway = new MessageReceiverGateway(hospitalRequestQueue);
        this.hospitalSerializer = new HospitalSerializer();
        this.requestToAggregationIdHashmap = new ConcurrentHashMap<>();

        this.messageReceiverGateway.setListener(hospitalRequestMessage -> {
            try {
                HospitalCostsRequest hospitalCostsRequest = this.hospitalSerializer
                        .requestFromString(((TextMessage) hospitalRequestMessage).getText());

                // Save the message aggregationId and corresponding hospital request.
                this.requestToAggregationIdHashmap.put(hospitalCostsRequest, hospitalRequestMessage.getStringProperty("aggregationId"));

                // Notify that a new request has been received.
                this.onRequestReceived(hospitalCostsRequest);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Sends a {@link HospitalCostsReply} to the provided request.
     *
     * @param hospitalCostsReply The reply to send.
     * @param toRequest          the request to which this reply belongs.
     */
    public void sendHospitalReply(HospitalCostsReply hospitalCostsReply, HospitalCostsRequest toRequest) {
        // Get the aggregation id of the message of the request to which this reply belongs.
        String aggregationId = this.requestToAggregationIdHashmap.get(toRequest);
        try {
            Message replyMessage = this.messageSenderGateway.createTextMessage(this.hospitalSerializer.replyToString(hospitalCostsReply));
            replyMessage.setStringProperty("aggregationId", aggregationId);
            this.messageSenderGateway.send(replyMessage);
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }

    /**
     * A callback triggered when a {@link HospitalCostsRequest} is received.
     *
     * @param hospitalCostsRequest The received request.
     */
    public void onRequestReceived(HospitalCostsRequest hospitalCostsRequest) {
        System.out.println("Received request: " + hospitalCostsRequest.toString());
    }
}
