package insurance.application;

import insurance.messaging.BrokerApplicationGateway;
import insurance.model.TreatmentCostsReply;
import insurance.model.TreatmentCostsRequest;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.apache.activemq.broker.Broker;

import javax.jms.JMSException;
import javax.naming.NamingException;
import java.net.URL;
import java.util.ResourceBundle;

public class InsuranceClientController implements Initializable {
    @FXML
    private ListView<ClientListLine> lvRequestsReplies;
    @FXML
    private TextField tfSsn;
    @FXML
    private TextField tfAge;
    @FXML
    private TextField tfTreatmentCode;
    @FXML
    private CheckBox cbTransport;
    @FXML
    private TextField tfKilometers;

    public BrokerApplicationGateway brokerApplicationGateway;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tfSsn.setText("123456");
        tfAge.setText("56");
        tfTreatmentCode.setText("ORT125");
        cbTransport.setSelected(false);
        tfKilometers.setDisable(true);

        // Initialize the broker application gateway
        try {
            this.brokerApplicationGateway = new BrokerApplicationGateway() {
                // This callback is triggered when a treatment cost reply has arrived.
                @Override
                public void onTreatmentReplyArrived(TreatmentCostsRequest request, TreatmentCostsReply reply) {
                    ClientListLine originalRequestLine = InsuranceClientController.this.getListLine(request);
                    originalRequestLine.setReply(reply);
                    Platform.runLater(() -> InsuranceClientController.this.lvRequestsReplies.refresh());
                }
            };
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private ClientListLine getListLine(TreatmentCostsRequest request) {
        for (ClientListLine clientListLine : lvRequestsReplies.getItems()) {
            if (clientListLine.getRequest() == request) {
                return clientListLine;
            }
        }
        return null;
    }

    public void transportChanged() {
        System.out.println(cbTransport.isSelected());
        if (!cbTransport.isSelected()) {
            tfKilometers.setText("");
        }
        this.tfKilometers.setDisable(!this.cbTransport.isSelected());
    }

    public void btnSendClicked() {
        int ssn = Integer.parseInt(this.tfSsn.getText());
        String treatmentCode = this.tfTreatmentCode.getText();
        int age = Integer.parseInt(this.tfAge.getText());

        int transportDistance = 0;
        if (this.cbTransport.isSelected()) {
            transportDistance = Integer.parseInt(this.tfKilometers.getText());
        }

        // Add request to list view.
        TreatmentCostsRequest request = new TreatmentCostsRequest(ssn, age, treatmentCode, transportDistance);
        ClientListLine requestListLine = new ClientListLine(request);
        this.lvRequestsReplies.getItems().add(requestListLine);

        // Send request.
        this.brokerApplicationGateway.sendTreatmentRequest(request);
    }
}
