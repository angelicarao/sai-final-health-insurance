package insurance.messaging;

import gateways.MessageReceiverGateway;
import gateways.MessageSenderGateway;
import insurance.model.TreatmentCostsReply;
import insurance.model.TreatmentCostsRequest;
import serializers.InsuranceSerializer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An application gateway providing functionality to communicate with a message broker.
 */
public class BrokerApplicationGateway {
    public MessageSenderGateway messageSenderGateway;
    public MessageReceiverGateway messageReceiverGateway;
    public InsuranceSerializer insuranceSerializer;
    private ConcurrentHashMap<String, TreatmentCostsRequest> requestHashMap;

    /**
     * Initializes a new instance of class {@link BrokerApplicationGateway}. Sets a message
     * listener which deals with received {@link TreatmentCostsReply} to sent
     * {@link TreatmentCostsRequest}.
     *
     * @throws JMSException    Thrown when an exception occurs in JMS.
     * @throws NamingException Thrown by JMS.
     */
    public BrokerApplicationGateway() throws JMSException, NamingException {
        this.messageSenderGateway = new MessageSenderGateway("requestInsuranceClient");
        this.messageReceiverGateway = new MessageReceiverGateway("replyInsuranceClient");
        this.insuranceSerializer = new InsuranceSerializer();
        this.requestHashMap = new ConcurrentHashMap<>();

        this.messageReceiverGateway.setListener(treatmentCostsReplyMessage -> {
            try {
                // Get the request for which this reply is.
                TreatmentCostsRequest originalRequest = this.requestHashMap
                        .get(treatmentCostsReplyMessage.getJMSCorrelationID());
                TreatmentCostsReply reply = this.insuranceSerializer
                        .replyFromString(((TextMessage) treatmentCostsReplyMessage).getText());

                // Trigger a callback with the received reply, saying for which request it is.
                this.onTreatmentReplyArrived(originalRequest, reply);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Sends a treatment cost request to the message broker.
     *
     * @param request The request to send.
     */
    public void sendTreatmentRequest(TreatmentCostsRequest request) {
        try {
            // Create message.
            Message requestMessage = this.messageSenderGateway
                    .createTextMessage(this.insuranceSerializer.requestToString(request));
            requestMessage.setJMSReplyTo(this.messageReceiverGateway.destination);

            // Send message.
            this.messageSenderGateway.send(requestMessage);

            // Save sent request in a hash map.
            this.requestHashMap.put(requestMessage.getJMSMessageID(), request);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    /**
     * A callback triggered when a reply s received in the reply queue.
     *
     * @param request The request for which the reply is.
     * @param reply   The reply to the request.
     */
    public void onTreatmentReplyArrived(TreatmentCostsRequest request, TreatmentCostsReply reply) {
        System.out.println("Received reply: " + reply.toString() + ", for request: " + request.toString());
    }
}
